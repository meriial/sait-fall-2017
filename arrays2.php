<pre>
<?php

$a = [
    'dog' => 'woof',
    'cat' => 'meow',
    'fox' => '???'
];

// echo $a['dog'] . ' or ' .$a['cat'];

$a['cow'] = 'moo';

unset($a['cat']);


foreach ($a as $key => $value) {
    echo "$key goes $value\n";
}
