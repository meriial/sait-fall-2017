<pre>
<?php

class Cake {
    public $flavour;

    public function eat() {
        return 'Yum! I love '.$this->flavour;
    }
}

$cake = new Cake();
$cake->flavour = 'chocolate';
echo $cake->eat();
echo "\n";
$cake2 = new Cake();
$cake2->flavour = 'carrot';
echo $cake2->eat();

// echo $cake->flavour;
