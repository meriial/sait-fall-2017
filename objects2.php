<pre>
<?php

class Cake {
    public $flavour;

    public function __construct($flavour) {
        $this->flavour = $flavour;
    }
}

$cake = new Cake('chocolate');
echo $cake->flavour."\n";
