<pre>
<?php

class Cake {
    public $flavour;
    public function eat()
    {
        return 'Yum, I love '.$this->flavour;
    }
}

class ChocolateCake extends Cake {
    public $flavour = 'chocolate';
}

$choc = new ChocolateCake();
echo $choc->eat();
