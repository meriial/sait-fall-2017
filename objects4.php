<?php

class Person {
    public $name;
    public $age;

    public function __construct($name, $age)
    {
        $this->name = $name;
        $this->age = $age;
    }
}

$array = [
    'name' => 'Bob',
    'age' => 24
];

echo $array['name']. ' is ' . $array['age'];

$bob = new Person('Bob', 24);

echo $bob->name . ' is ' . $bob->age;
